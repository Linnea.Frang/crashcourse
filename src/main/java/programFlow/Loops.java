package programFlow;

import java.util.Arrays;
import java.util.List;

public class Loops {

	public static void main(String[] args) {
		int n = 0;
		while (n < 10) {
			System.out.println(n);
			n += 1;
		}
		//----------------------------
		for (int i = 0; i < 5; i++) {
			System.out.println(i);
		}
		//----------------------------
		List<Integer> integerList = Arrays.asList(0, 1, 2, 3, 4);
		for (Integer integer : integerList) {
			System.out.println(integer);
		}
		
		//---------------------------
		int counter = 0;
		while (true) {
			if (counter > 5) {
				break;
			}
			System.out.println("kake");	
		}
		//----------------------------
		for (int i = 0; i < 5; i++) {
			if (i % 2 == 0) {
				continue;
			}
			System.out.println(i);
		}
	}
}
